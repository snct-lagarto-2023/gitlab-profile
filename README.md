<div align="center">

<img src="./assets/img/logo_SNCT_23_HORIZONTAL.png" alt="snct-logo" width="25%">

# SNCT IFS/LAGARTO 2023

Este repositório tem a função de armazenar parte do material que será utilizado nos mini cursos durante o evento da
semana nacional de ciência e tecnologia (SNCT) que acontecerá no campus Lagarto do IFS.

</div>

## Introdução

Boas-vindas ao repositório da SNCT IFS/LAGARTO 2023. Este não é o repositório oficial, mas sim aquele administrado pelos
estudantes do IFS. Aqui, você terá acesso a materiais e informações essenciais relacionadas ao evento.

## O que é SNCT?

A Semana Nacional de Ciência e Tecnologia (SNCT) é um evento anual realizado no Brasil pelo Ministério da Ciência,
Tecnologia e Inovação (MCTI) em parceria com instituições públicas e privadas. O objetivo da SNCT é promover a ciência e
a tecnologia para a população, mostrando sua importância para o desenvolvimento do país e para a melhoria da qualidade
de vida das pessoas.

## Local

O evento será realizado no campus Lagarto do Instituto Federal de Sergipe (IFS).

## Eventos

Confira a seguir os principais eventos que serão conduzidos pelos colaboradores deste repositório.

- [Clique neste link para se registrar nos eventos](http://intranet.ifs.edu.br/publicacoes//site/indexEventoDetalhado.wsp?tmp.evento.id_evento=2137)

### 1. Introdução ao Hyperf: Um guia básico para iniciantes

Neste minicurso, você terá a oportunidade de aprender o básico sobre o Hyperf, uma tecnologia de destaque.

Nesse minicurso, exploraremos o contexto histórico das aplicações PHP, onde a ausência de suporte para código assíncrono
impactava negativamente o desempenho e a escalabilidade dos projetos. Abordaremos o surgimento do framework Hyperf, uma
solução inovadora para lidar com esses desafios de programação assíncrona.

Também discutiremos as empresas que já adotaram o Hyperf para o desenvolvimento de suas aplicações web, destacando sua
relevância no mercado atual. Além disso, vou guiar vocês na criação de um projeto simples em Hyperf, utilizando o Docker
para configurar o ambiente. Esse processo envolverá a execução de comandos específicos para gerar a estrutura inicial e
o arquivo de configuração.

Por fim, compartilharei algumas dicas valiosas para o estudo do Hyperf, visando aprofundar seu conhecimento nesse
framework promissor. Esteja preparado para descobrir como o Hyperf pode impulsionar a eficiência, escalabilidade e
desempenho de suas aplicações web.

- **Evento**: SNCT-LAGARTO 2023
- **Sub-Evento**: Introdução ao Hyperf: Um guia básico para iniciantes
- **Tipo do Sub-Evento**: Mini curso
- **Quantidade vagas disponíveis**: 0
- **Ministrante/Palestrante**: [Reinan Gabriel Dos Santos Souza](https://linktr.ee/reinanhs)
- **Data(s)**: 25/10 das 19h30 às 21h30


### 2. Introdução sobre Observabilidade em Sistemas Distribuídos

Nesse minicurso, vamos explorar o conceito de observabilidade e como é possível monitorar o comportamento interno de
sistemas complexos. Uma vez que através da observabilidade, podemos obter insights valiosos sobre o estado, desempenho e
interações dos componentes de um sistema distribuído por meio de seus sinais externos.

Ao contrário de simplesmente verificar o funcionamento de um sistema, a observabilidade vai além, proporcionando uma
compreensão profunda de como os diferentes elementos se comportam e se relacionam entre si. Isso envolve a coleta e
análise de métricas, registros detalhados, rastreamento de transações e eventos, além da criação de visualizações claras
e intuitivas dos dados coletados.

Além disso, destacarei algumas das principais ferramentas de observabilidade disponíveis, como o Prometheus para
monitoramento de infraestrutura, e o Jaeger e o Zipkin para rastreamento distribuído. Essas ferramentas oferecem
visibilidade e compreensão do comportamento de sistemas distribuídos, permitindo a identificação de gargalos, solução de
problemas e otimização de desempenho.

- **Evento**: SNCT-LAGARTO 2023
- **Sub-Evento**: Introdução sobre observabilidade em sistemas distribuídos
- **Tipo do Sub-Evento**: Mini curso
- **Quantidade vagas disponíveis**: 0
- **Ministrante/Palestrante**: [Reinan Gabriel Dos Santos Souza](https://linktr.ee/reinanhs)
- **Data(s)**: 24/10 das 19h30 às 21h30

## Changelog

Por favor, veja [CHANGELOG](CHANGELOG.md) para obter mais informações sobre o que mudou recentemente.

## Seja um dos contribuidores

Sinta-se à vontade para contribuir com melhorias, correções de bugs ou adicionar recursos a este repositório. Basta
criar um fork do projeto, fazer as alterações e enviar um pull request. Suas contribuições serão bem-vindas!

Quer fazer parte desse projeto? leia [como contribuir](CONTRIBUTING.md).

## Licença

Este projeto é licenciado sob a Licença MIT. Veja o arquivo [LICENÇA](LICENSE) para mais detalhes.
